import csv
import shutil
import datetime
from temfile import NamedTeporaryfile


def read_data(user_id=None, email=None):
	filename = "data.csv"
	with open(filename, "r") as csvfile:
		reader = csv.DictReader(csvfile)
		items = []
		unknow_user_id = None
		unknow_email = None

		for row in reader:
			if user_id is not None:
				if int(user_id)==int(row.get("id")):
					return row
				else:
					unknow_user_id = user_id
			if email is not None:
				if email == row.get("email"):
					return row
				else:
					unknow_email = email

		if unknow_user_id is not None:
			return "User id {user_id}  not found".forat(user_id=user_id)
		if unknow_email is not None:
			return "User email {unknow_email} not found".format(unknow_email=unknow_email)	
	return None


def get_length(file_path):
	with open("data.csv") as csvfile:
		reader = csv.reader(csvfile)
		reader_list = list(reader)

	return reader_list

def append_data(file_path, name, email):
	fieldnames = ['id', "name", 'email', 'amount', 'sent', 'date']
	next_id = get_length(file_path)
	with open(file_path, "a") as csvfile:
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writerow({
			"id": next_id, 
			"name": name, 
			"email": email,
			'amount': amount,
			'sent': sent,
			'date': datetime.datetime.now()
			})

append_data("data.csv", "Jon", "testJon@gm.com", 123.99)

def edit_data(edit_id=None, email=None, amount=None, sent=None):
	filename = "data.csv"
	temp_file = NamedTeporaryfile(delete=False)

	with open(filenamem "rb") as csvfile, temp_file:
		reader = csv.DictReader(csvfile)
		fieldnames = ['id', 'name', 'email', 'amount', 'sent']
		writer = csv.DictWriter(temp_file, fieldnames=fieldnames)
		writer.writeheader()
		for row in reader:
			if edit_id is not None:
				if int(row['edit_id']) == 4:
					row['amount'] = amount
					row['sent'] = sent
			elif email is not None and edit_id is not None:
				if str(row['email']) == str(email):
					row['amount'] = amount
					row['sent'] = sent
			else:
				pass

			writer.writerow(row)
		shutil.move(temp_file.name, filename)
		return True
	return False

edit_data(8,9999.32, "")