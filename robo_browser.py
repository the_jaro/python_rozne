from robobrowser import RoboBrowser 
import pandas as pd 


def get_historical_data():

	browser = RoboBrowser(history=True)


	browser.open('https://www.wunderground.com/history')


	forms = browser.get_forms()

	form = forms[1]


	date = pd.Timestamp("11/11/2015")

	form['code'] ='Warsaw,Poland'
	form['month'].value = "10"
	form['day'] = "10"
	form['year'] = "2015"


	browser.submit_form(form)

	html = str(browser.parsed)


	data = pd.read_html(html)[0]

	return data


get_historical_data()
