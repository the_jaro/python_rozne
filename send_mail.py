from email.mime.multipart iport MINEMultipart
from email.mime.text import MINEText
from smtplib import SMTP, SMTPAuthenticationError, SMTPException
import config


host = "smtp.gmail.com"
port = 587
username = config.username
password = config.password
from_email = username
to_list = ["hungrypy@gmail.com"]
try:
	email_conn = SMTP(host, port)
	email_conn.ehlo()
	email_conn.starttls()

	email_conn.login(username, password) 


	the_msg = MINEMultipart("alternative")
	the_msg['subject'] = "Test message"
	the_msg["To"] = to_list

	plain_txt = "Testing the message"
	html_txt = """
	<html>
		<head></head>
		<body>
			<p>Hey</br>
			Testing mail
			</p>
		</body>
	</html>
	"""

	part_1 = MIMEText(plain_txt, 'plain')
	part_2 = MINEText(html_txt, "html")

	the_msg.attach(part_1)
	the_msg.attach(part_2)

	email_conn.sendmail(from_email, to_list, the_msg.as_string())
	email_conn.quit()
except SMTPException:
	print("error, couldn't send message")